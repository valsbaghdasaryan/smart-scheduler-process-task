package com.company;

import com.company.Algoritms.BaseScheduler;
import com.company.Algoritms.SchedulerEDF;
import com.company.Algoritms.SchedulerFIFO;
import com.company.Algoritms.SchedulerGreedy;
import com.company.InputOutput.InputReader;

import java.util.ArrayList;

public class SchedulerController {

    private static final int MAX_TIME = 25;
    private ArrayList<SProcessor> processors;
    private ArrayList<STask> tasks;

    private BaseScheduler scheduler;

    /**
     * The global maximum power that should not exceed
     */
    public static float GLOBAL_CAP = 1000;
    public static float ENERGY_CAP = 10000;
    public static int NUMBER_OF_REPEAT = 2;


    public SchedulerController(String[] args)
    {
        this.processors = new ArrayList<>();
        this.tasks = new ArrayList<>();


        InputReader reader = new InputReader();
        reader.readProcessorsTxt(this.processors);
        reader.readTasksTxt(this.tasks);


        if(args.length > 0)
        {
            if(args[0].equals("1"))
            {
                scheduler = new SchedulerFIFO(this.processors, this.tasks);
            }
            else if(args[0].equals("2"))
            {
                scheduler = new SchedulerEDF(this.processors, this.tasks);
            }
            else {
                scheduler = new SchedulerGreedy(this.processors, this.tasks);
            }
        }
        else {
            scheduler = new SchedulerGreedy(this.processors, this.tasks);
        }
/*

        // FIFO
        scheduler = new SchedulerFIFO(this.processors, this.tasks);
        scheduler.run();
        // EDF
        scheduler = new SchedulerEDF(this.processors, this.tasks);
        scheduler.run();
*/

        //GREEDY
//        scheduler = new SchedulerGreedy(this.processors, this.tasks);
        scheduler.run();
    }

}
