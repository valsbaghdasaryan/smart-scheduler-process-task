package com.company;

import com.company.InputOutput.Reporter;

public class STime {
    /**
     * ***********************************************************************
     * ******************************ASSUMPTIONS******************************
     * ***********************************************************************
     *
     * performance of processor >= 1
     * minFrequency of each processor >= 1
     *
     *  THE task id and the order of task in test textfile should be same FOR DEPENDENCIES
     */

    /**
     * THE GLOBAL TIME OF SCHEDULER
     */
    public static int time;

    public static void init()
    {
        time = -1;
    }

    public static void update()
    {
        time ++;
        Reporter.printTime(time);
    }

}
