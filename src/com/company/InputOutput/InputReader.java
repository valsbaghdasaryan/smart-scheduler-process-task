package com.company.InputOutput;

import com.company.SProcessor;
import com.company.STask;
import com.company.SchedulerController;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

public class InputReader {

    private static  String PREF_PROCESSORS = "/servers.txt";
    private static  String PREF_JOBS = "/jobs.txt";
    private static  String PREF_DEP = "/dependencies.txt";

    private static String PREF_MAIN_TEST_FILE = "/test.txt";


    public static String mainPath;



    public InputReader()
    {
        String filePath = new File("").getAbsolutePath();
        filePath = filePath.concat("/inputs");
        readTestFile(filePath + PREF_MAIN_TEST_FILE);

        this.mainPath = filePath+ "/";



    }

    public void readTestFile(String mainFile)
    {
        try {
            File file = new File(mainFile);
            FileReader reader = new FileReader(file);
            BufferedReader bReader = new BufferedReader(reader);

            String line = bReader.readLine();
            while (line != null) {
                if (line.length() == 0) {
                    line = bReader.readLine();
                    continue;
                }
                if (line.charAt(0) == '#') {
                    line = bReader.readLine();
                    continue;
                }

                if(line.contains("job_file"))
                {
                    PREF_JOBS = "/" + line.split("\"")[1];
                }

                if(line.contains("dependency_file"))
                {
                    PREF_DEP = "/" + line.split("\"")[1];
                }

                if(line.contains("server_file"))
                {
                    PREF_PROCESSORS = "/" + line.split("\"")[1];
                }

                if(line.contains("power_cap"))
                {
                    SchedulerController.GLOBAL_CAP = Float.parseFloat(line.split(" ")[2]);
                }

                if(line.contains("energy_cap"))
                {
                    SchedulerController.ENERGY_CAP = Float.parseFloat(line.split(" ")[2]);
                }

                if(line.contains("repeat"))
                {
                    SchedulerController.NUMBER_OF_REPEAT = Integer.parseInt(line.split(" ")[2]);
                }

                line = bReader.readLine();
            }
        }
        catch (Exception e)
        {
            e.getStackTrace();
        }
    }


    public void readTasksTxt(ArrayList<STask> tasks)
    {
        try {

            File file = new File(this.mainPath + PREF_JOBS);
            FileReader reader = new FileReader(file);
            BufferedReader bReader = new BufferedReader(reader);

            String line;
            while((line = bReader.readLine()) != null)
            {
                if(line.length() == 0 || line.charAt(0) == '#')
                {
                    continue;
                }

                String[] first = line.split(" ");

                tasks.add(new STask(
                        Integer.parseInt(first[0]), // ID
                        Integer.parseInt(first[1]), // Arrival time/date
                        Integer.parseInt(first[2]), // units of work
                        Integer.parseInt(first[3]), // deadline
                        Integer.parseInt(first[4]), // period
                        Float.parseFloat(first[5])) // power consumption
                );
            }

            reader.close();
            bReader.close();



            file = new File(this.mainPath + PREF_DEP);
            reader = new FileReader(file);
            bReader = new BufferedReader(reader);

            while((line = bReader.readLine()) != null) {
                if (line.length() == 0 || line.charAt(0) == '#') {
                    continue;
                }

                String[] first = line.split(" - ");
                int parent = Integer.parseInt(first[0]);
                int child = Integer.parseInt(first[1]);
                addDependence(tasks, parent, child);
            }

            reader.close();
            bReader.close();

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void addDependence(ArrayList<STask> tasks, int parentId, int childId)
    {
        // in this function we assume that the task id is the same as order in array/text file
        tasks.get(childId).setParent(tasks.get(parentId)); // TODO
    }


    public void readProcessorsTxt(ArrayList<SProcessor> processors)
    {
        try {
            File file = new File(this.mainPath + PREF_PROCESSORS);
            FileReader reader = new FileReader(file);
            BufferedReader bReader = new BufferedReader(reader);

            String line = bReader.readLine();
            while(line !=null  )
            {

                if(line.length() == 0)
                {
                    line = bReader.readLine();
                    continue;
                }
                if(line.charAt(0) == '#')
                {
                    line = bReader.readLine();
                    continue;
                }

                String[] first ;
                String[] temp;
                String[] second;
                String[] third;
                //int id, float staticPow, int performane, float[] frequencies, float localPowerConsumption
                float[] freqs;


                line = line.replace("(", ",");              //0 20 1 (1 1.5 2 3) 100
                line = line.replace(")", ",");              //0 20 1 ,1 1.5 2 3, 100

                temp = line.split(",");
                first = temp[0].split(" ");
                second = temp[1].split(" ");

                freqs = new float[second.length];
                for(int i = 0 ; i < second.length; i++)
                {
                    freqs[i] = Float.parseFloat(second[i]);
                }
                third = temp[2].split(" ");

                processors.add(new SProcessor(
                        Integer.parseInt(first[0]),
                        Float.parseFloat(first[1]),
                        Integer.parseInt(first[2]),
                        freqs,
                        Float.parseFloat(third[1])));

                first = null; second = null; third = null; temp = null;


                line = bReader.readLine();
            }

            reader.close();
            bReader.close();

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }





}
