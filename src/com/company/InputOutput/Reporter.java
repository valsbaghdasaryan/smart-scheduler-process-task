package com.company.InputOutput;

import com.company.SProcessor;
import com.company.STask;
import com.company.STime;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.List;

public class Reporter {

    public static String PREFIX_OUTPUT = "result.txt";
    public static String PREFIX_LOG = "logFile.txt";

    private static int nbOfViolations;
    private static String result;
    private static String logFile;


    public static void init()
    {
        nbOfViolations = 0;
        result = "";
        logFile = "";
    }

    public static void finish(String algoName ,float energyConsumption, float maxPowerConsumption)
    {
        String finalWord = "\n*********************END of " + algoName + " ********************************************" +
                "\nDEADLINE VIOLATION:" + nbOfViolations + ", ENERGY_CONSUMPTION:" +
                energyConsumption + ", MAX POWER CONSUMPTION" + maxPowerConsumption + "\n";

        print(finalWord);


        String path = InputReader.mainPath;
        File folder = new File(path + "/" + algoName);
        if(!folder.exists())
        {
            folder.mkdir();
        }
        File result = new File(path + "/" + algoName + "/" +PREFIX_OUTPUT);
        File log = new File(path + "/"  + algoName + "/" +PREFIX_LOG);

        try {
            FileWriter fWriter = new FileWriter(result);
            BufferedWriter bWrieter = new BufferedWriter(fWriter);
            bWrieter.append(Reporter.result);
            bWrieter.close();
            fWriter.close();
            fWriter = new FileWriter(log);
            bWrieter = new BufferedWriter(fWriter);
            bWrieter.append(Reporter.logFile);
            bWrieter.close();
            fWriter.close();

            String[] cmd = new String[3];
            cmd[0] = "python";
            cmd[1] = result.getParentFile().getParentFile().getParent() + "\\plotter.py";
            cmd[2] = result.getAbsolutePath();

            System.out.println();
            System.out.println(cmd[0]);
            System.out.println(cmd[1]);

            Process p = Runtime.getRuntime().exec(cmd);

        }catch (Exception e){
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public static void taskAssigned(SProcessor proc, STask task)
    {
        print("_START TASK " + task.representativeID() + " :PROC " + proc.id + " :FREQ " + proc.currentFrequency);

        float powerCons = proc.powerConsumption();
        if(powerCons > proc.localPowerConsumptionCap)
        {
            localPowerCapViolation(proc, task, powerCons, proc.localPowerConsumptionCap);
        }
        println();
    }

    public static void taskFinished(SProcessor proc, STask task)
    {
        print("___END TASK " + task.representativeID() + " :PROC " + proc.id);
        result += task.id + " " + proc.id + " " + task.startTime + " " + STime.time + "\n";
        if(STime.time > task.deadline)
        {
            deadlineViolation(proc, task);
        }


        println();
    }

    public static void localPowerCapViolation(SProcessor proc, STask task, float current, float cap)
    {
        print("_____LOCAL_VIOLATION TASK" + task.representativeID() + " :PROC " + proc.id  + " _ Current/Cap = " + current + " / " + cap);
    }

    public static void globalPowerCapViolation(List<SProcessor> procs, float globalPower)
    {
        print(" ####### GLOBAL VIOLATION ");
        print(procs);
        print( globalPower);
    }

    public static void deadlineViolation(SProcessor proc , STask task)
    {
        nbOfViolations ++;
        print(" : TS" + (task.deadline - STime.time) + " :TOTAL NUMBER VIOLATION " + nbOfViolations);
    }

    private static void print(String str)
    {
        System.out.print(str);
        logFile += str;
    }

    public static void printTime(int time)
    {
        String timeStr = "TIME ___ " + time;
        System.out.println(timeStr);
        logFile += timeStr + "\n";
    }
    
    private static void print(Object obj)
    {
        logFile += obj.toString();
        System.out.print(obj);
    }

    private static void println()
    {
        logFile += "\n";
        System.out.println();
    }

}
