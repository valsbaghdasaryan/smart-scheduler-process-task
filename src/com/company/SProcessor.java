package com.company;

import com.company.Algoritms.Components.Costs;
import com.company.InputOutput.Reporter;

public class SProcessor {

    public int id;

    /**
     * static power consumption for the minimum available frequency
     */
    public float staticPower;
    /**
     * number of unit of work per time step at minimum frequency
     */
    public int performance;

    /**
     * list of values of frequencies that can be performed for current processor
     */
    public float[] frequencies;

    /**
     * The frequency coefficient
     */
    public float currentFrequency;

    /**
     * The max amount of power can be used by current processor
     */
    private STask runningTask;

    public float localPowerConsumptionCap;

    public SProcessor(int id, float staticPow, int performane, float[] frequencies, float localPowerConsumptionCap)
    {
        this.id = id;
        this.staticPower =staticPow;
        this.performance = performane;
        this.frequencies = frequencies;
        this.localPowerConsumptionCap = localPowerConsumptionCap;

        this.currentFrequency = 1;
    }

    public void addTask(STask task, float freq)
    {
        currentFrequency = freq;
        this.runningTask = task;
        this.runningTask.start();
        Reporter.taskAssigned(this, runningTask);


    }

    public STask timeUpdate()
    {
        if(this.runningTask == null)
        {
            return null;
        }
        this.runningTask.workToDo -= this.performance * this.currentFrequency;

        if(this.runningTask.isFinished())
        {
            Reporter.taskFinished(this, this.runningTask);

            STask finishedTask = this.runningTask;

            this.runningTask = null;

            // set current freq minimum for power consumption
            currentFrequency = frequencies[0];

            return finishedTask;
        }
        return null;
    }


    public float powerConsumption()
    {
        if(!isFree())
        {
            float amount = (this.staticPower + this.runningTask.power ) * currentFrequency;

            return amount;
        }
        return staticPower * frequencies[0];
    }

    public boolean isFree()
    {
        return  (this.runningTask == null);
    }

    /**
     * The value of cost to compare the different frequencies of processor - task pair
     * @param frequency frequency for which the cost value will be calculated
     * @param task for which the cost value will be calculated
     * @return need to minimize this value
     */
    public float costForPerformingTask(float frequency, STask task)
    {
        int currentTime = STime.time;
        float power = (this.staticPower + task.power) * frequency;
        float cost = 0;
        float time = task.unitsOfWork / ( this.performance * frequency);
        int deadlineLeft = task.deadline - (currentTime + (int)Math.ceil(time));



        // if power consumes more than cap, increase cost for 10000
        if(power > this.localPowerConsumptionCap)
        {
            cost += 10000 + power/this.localPowerConsumptionCap * Costs.POWER_WEIGHT;

            if(deadlineLeft > 0)
            {
                cost += (time/(task.deadline -currentTime))*Costs.TIME_WEIGHT;
            } else if(deadlineLeft < 0)
            {
                cost += 1000 + (-1/deadlineLeft)*Costs.TIME_WEIGHT;// deadline sign is minus
            }
        }
        else {
            if(deadlineLeft > 0)
            {
                cost += ((time/(task.deadline -currentTime)))*Costs.TIME_WEIGHT;
            }
            else if(deadlineLeft < 0)
            {
                cost += 1000 + (-1/deadlineLeft)*Costs.TIME_WEIGHT;// deadline sign is minus

            }


        }

        if(power > SchedulerController.GLOBAL_CAP)
        {
            cost += 100000;
        }


        // if task have children it is better to perform fast
        cost -=  task.numberOfChildren*Costs.CHILDREN_WEIGHT;
        return cost;
    }

    public float computePowerConsumption(float freq, STask task)
    {
        return (this.staticPower + task.power) * freq;
    }


}
