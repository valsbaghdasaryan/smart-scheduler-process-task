package com.company;



/**
* Scheduler Task
* */
public class STask {

    public int id;
    public int numberOfChildren;
    public int startTime;
    public int workToDo;

    public int deadline;
    /**
     * Dynamic power
    * */
    public float power;

    /**
    *  Units of work
    * */
    public int unitsOfWork;

    /**
     * The task must be executed each @period time
     * */
    public int period;

    /**
     * The task will arrive at the certain time @arrivalTime
     */
    public int arrivalTime;

    /**
     * The task that should be finished, before the current starts
     * */
    public STask parent;

    /**
     * if this task is clone , than @originID is the id of origin task, from which it is cloned
     */
    private int originId;

    public STask(int id, int arrivalTime, int unitsOfWork, int deadline, int period, float power)
    {
        this.id = id;
        this.deadline = arrivalTime + deadline;
        this.unitsOfWork = unitsOfWork;
        this.power = power;
        this.arrivalTime = arrivalTime;
        this.period = period;

        this.workToDo = unitsOfWork;

        this.numberOfChildren = 0;

        this.startTime = 0;

        this.originId = -1;
    }

    public void setAsClone(int originId)
    {
        this.originId = originId;
    }

    public String representativeID()
    {
        if(this.originId >= 0)
        {
            return this.originId + "P";
        }
        return this.id +"";
    }

    public void setParent(STask pTask)
    {
        this.parent = pTask;
        numberOfChildren ++;
    }

   public void start()
   {
       this.startTime = STime.time;
       this.workToDo = this.unitsOfWork;
   }

    /**
     * Assumption: For each proc minimum freq = 1 and minimum performance = 1
     * @return the time left for deadline end after finishing this task using proc with 1 performance and 1 freq
     */
   public int deadlineCapCoefficient()
   {
       return this.deadline - STime.time - this.unitsOfWork;
   }

    public boolean isFinished()
    {
        return (this.workToDo <= 0);
    }

}
