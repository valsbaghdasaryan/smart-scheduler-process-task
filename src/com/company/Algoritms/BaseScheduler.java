package com.company.Algoritms;

import com.company.Algoritms.Components.PairProcessorTask;
import com.company.Algoritms.Components.Solution;
import com.company.InputOutput.Reporter;
import com.company.SProcessor;
import com.company.STask;
import com.company.STime;
import com.company.SchedulerController;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public abstract class BaseScheduler
{
    protected static final int MAX_TIME = 25;
    protected ArrayList<SProcessor> processors;
    protected ArrayList<STask> tasks;

    public BaseScheduler(List<SProcessor> processors, List<STask> tasks)
    {
        this.processors = new ArrayList<>(processors);
        this.tasks = new ArrayList<>(tasks);
//        Collections.copy( processors, this.processors);
//        Collections.copy( tasks, this.tasks);


    }

    public void run()
    {
        int i;
        SProcessor proc;
        STask task;
        ArrayList<STask> arrivedTasked = new ArrayList<>();
        ArrayList<STask> inProgressTasks = new ArrayList<>();

        int pLength = this.processors.size();

        Reporter.init();

        System.out.println("**********************************************************");
        System.out.println("************************ " + name() + " *******************");
        System.out.println("**********************************************************");
        STime.init();

        float energyConsumption = 0;
        float maxPowerConsumption = Float.MIN_VALUE;

        while(tasks.size() > 0 || arrivedTasked.size() > 0 || inProgressTasks.size() > 0)
        {
            STime.update();


            /**
             * Task arrival
             */
            for(i = 0; i < tasks.size(); i++)
            {
                task = tasks.get(i);
                if(task.arrivalTime == STime.time)
                {
                    arrivedTasked.add(task);
                    if(task.period > 0)
                    {
                        duplicatePeriodicTask(task);
                    }
                    tasks.remove(i);
                    i--;
                }
            }


            /**
             * Task finished
             */
            float currentPowerConsumption = 0;
            for(i = 0; i < pLength; i ++)
            {
                proc = this.processors.get(i);
                if(!proc.isFree())
                {
                    task = proc.timeUpdate();

                    if(task != null) // task has finished
                    {
                        inProgressTasks.remove(task);
                    }
                }
                currentPowerConsumption += proc.powerConsumption();

            }
            energyConsumption += currentPowerConsumption;
            if(currentPowerConsumption > maxPowerConsumption  )
            {
                maxPowerConsumption = currentPowerConsumption;
            }

            if(currentPowerConsumption > SchedulerController.GLOBAL_CAP)
            {
                Reporter.globalPowerCapViolation(this.processors, currentPowerConsumption);
            }

            /**
             * Getting free processors
             */
            ArrayList<SProcessor> freeProcs = new ArrayList<>();
            for(i = 0; i < pLength; i ++)
            {
                proc = this.processors.get(i);
                if(proc.isFree())
                {
                    freeProcs.add(proc);
                }
            }


            /**
             * if there is NO FREE PROC or there is NO TASK to assign, continue (time update)
             */
            if(freeProcs.size() == 0 || arrivedTasked.size() == 0)
            {
                continue;
            }

            List<STask> independentTasks = new ArrayList<>();
            for(i = 0 ; i < arrivedTasked.size(); i++)
            {
                task = arrivedTasked.get(i);
                // if the task has not dependence or his dependence is already finished
                if(task.parent == null || (findTask(arrivedTasked, task.parent.id)<0 && findTask(inProgressTasks, task.parent.id) < 0))
                {
                    independentTasks.add(task);
                }
            }



            if(independentTasks.size() == 0) continue;

            Solution sol = getBestSolution( freeProcs, independentTasks);

            for(i = 0; i < sol.pairs.size(); i++)
            {
                PairProcessorTask pair = sol.pairs.get(i);
                pair.processor.addTask(pair.task, pair.optimalFrequency);
                inProgressTasks.add(pair.task);
                arrivedTasked.remove(pair.task);
            }
        }
        Reporter.finish(name(), energyConsumption, maxPowerConsumption);
    }

    protected void duplicatePeriodicTask(STask task)
    {
        int header = (int)Math.pow(10, (int)Math.log10(tasks.size()) + 1);
        if(header == 10){header = 100;}

        for(int i = 0; i < SchedulerController.NUMBER_OF_REPEAT - 1; i++)
        {
            STask clone = new STask(
                    header+ task.id ,
                    task.arrivalTime + (i+1)*task.period,
                    task.unitsOfWork,
                    task.deadline + (i+1)*task.period,
                    0,
                    task.power);
            clone.setAsClone(task.id);
            if(i == 0)
            {
                clone.setParent(task);
            }
            else {
                clone.setParent(tasks.get(tasks.size() -1));
            }
            tasks.add(clone);
        }
    }



    protected SProcessor findProcessor(ArrayList<SProcessor> processors, int procID)
    {
        for(int i = 0; i < processors.size(); i++)
        {
            SProcessor proc;
            if((proc = processors.get(i)).id == procID)
            {
                return proc;
            }
        }
        return  null;
    }

    private int findTask(List<STask> tasks, int taskID)
    {
        for(int i = 0; i < tasks.size(); i++)
        {
            if(tasks.get(i).id == taskID)
            {
                return i;
            }
        }
        return  -1;
    }

    protected List<List<Integer>> permute(int[] nums) {
        List<List<Integer>> results = new ArrayList<List<Integer>>();
        if (nums == null || nums.length == 0) {
            return results;
        }
        List<Integer> result = new ArrayList<>();
        dfs(nums, results, result);
        return results;
    }

    private void dfs(int[] nums, List<List<Integer>> results, List<Integer> result) {
        if (nums.length == result.size()) {
            List<Integer> temp = new ArrayList<>(result);
            results.add(temp);
        }
        for (int i=0; i<nums.length; i++) {
            if (!result.contains(nums[i])) {
                result.add(nums[i]);
                dfs(nums, results, result);
                result.remove(result.size() - 1);
            }
        }
    }



    protected abstract String name();
    protected abstract Solution getBestSolution(ArrayList<SProcessor> processors, List<STask> independentArrivedTasks);

}
