package com.company.Algoritms.Components;

import com.company.SProcessor;
import com.company.STask;

public class PairProcessorTask {

    public STask task;
    public SProcessor processor;

    public float optimalFrequency;

    public PairProcessorTask(STask task, SProcessor processor)
    {
        this.task = task;
        this.processor = processor;
    }

    public PairProcessorTask()
    {
    }


    public float computeCost(float freq)
    {
        return processor.costForPerformingTask( freq, task);
    }

    public float powerConsumption(float freq)
    {
        return processor.computePowerConsumption( freq, task);
    }
}
