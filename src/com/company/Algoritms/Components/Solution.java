package com.company.Algoritms.Components;

import com.company.SchedulerController;

import java.util.ArrayList;
import java.util.List;

public class Solution {

    public List<PairProcessorTask> pairs;
    public List<List<Float>> pairSolutions;

    public Solution(List<PairProcessorTask> pairs)
    {
        this.pairs = pairs;
    }
    public float getCost()
    {
        pairSolutions = new ArrayList<>();

        generatePairSolutionsMap(pairSolutions, new ArrayList<>(pairs.size()));

        int minCostIndex = -1;
        float minCost = Float.MAX_VALUE;
        float minPower = Float.MAX_VALUE;
        int minPowerIndx = -1;

        float costs[] = new float[pairSolutions.size()];
        float power[] = new float[pairSolutions.size()];
        for(int i = 0; i < pairSolutions.size(); i++)
        {
            List<Float> result = pairSolutions.get(i);
            costs[i] = 0;
            power[i] = 0;
            for(int pair = 0; pair < pairs.size(); pair++)
            {
                float freq = result.get(pair);
                costs[i] += pairs.get(pair).computeCost(freq);
                power[i] += pairs.get(pair).powerConsumption(freq);
            }

            if(costs[i] < minCost && power[i] < SchedulerController.GLOBAL_CAP){
                minCost = costs[i];
                minCostIndex = i;
            }

            if(power[i] < minPower)
            {
                minPower = power[i];
                minPowerIndx = i;
            }
        }

        int solIndex;
        if(minCost == Float.MAX_VALUE)// case when all solutions exceed the GLOBAL
        {
            solIndex = minPowerIndx;
        }
        else {
            solIndex = minCostIndex;
        }

        for(int i = 0; i < pairs.size(); i++)
        {
            PairProcessorTask pair = pairs.get(i);
            pair.optimalFrequency = pairSolutions.get(solIndex).get(i);
        }
        return costs[solIndex];
    }

    private void generatePairSolutionsMap(List<List<Float>> results, List<Float> result)
    {
        if(result.size() == pairs.size())
        {
            results.add(new ArrayList<>(result));
            return;
        }
        PairProcessorTask pair = this.pairs.get(result.size());
        {
            for(int i = 0; i < pair.processor.frequencies.length; i++)
            {
                result.add(pair.processor.frequencies[i]);
                generatePairSolutionsMap(results, result);
                result.remove(result.size() - 1);
            }
        }
    }

}
