package com.company.Algoritms;

import com.company.Algoritms.Components.PairProcessorTask;
import com.company.Algoritms.Components.Solution;
import com.company.SProcessor;
import com.company.STask;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SchedulerEDF  extends BaseScheduler{
    public SchedulerEDF(ArrayList<SProcessor> processors, ArrayList<STask> tasks) {
        super(processors, tasks);
    }

    @Override
    protected String name() {
        return "SchedulerEDF";
    }

    @Override
    protected Solution getBestSolution(ArrayList<SProcessor> processors, List<STask> independentArrivedTasks) {
        /**
         * We are sure that after sorting all the tasks will be sorted by their "arrivalDate" value (FIFO)
         */
        Collections.sort(independentArrivedTasks, new Comparator<STask>() {
            @Override
            public int compare(STask o1, STask o2) {
                return Integer.compare(o1.deadline, o2.deadline);
            }
        });


        List<PairProcessorTask> pairs = new ArrayList<>();

        for(int i = 0; i < independentArrivedTasks.size(); i++)
        {
            STask task = independentArrivedTasks.get(i);
            PairProcessorTask bestPair = new PairProcessorTask();
            boolean isPairInitialized = false;


            for(int j = 0; j < processors.size(); j++)
            {
                SProcessor proc = processors.get(j);
                PairProcessorTask pair = new PairProcessorTask();
                pair.task = task;
                pair.processor = proc;


                float minCostForFreq = Float.MAX_VALUE;

                for(float freq : proc.frequencies)
                {
                    float current = pair.computeCost(freq);
                    if(current < minCostForFreq)
                    {
                        minCostForFreq = current;
                        pair.optimalFrequency = freq;
                    }
                }

                if(!isPairInitialized){
                    bestPair = pair;
                    isPairInitialized = true;
                }
                else if(pair.computeCost(pair.optimalFrequency) < bestPair.computeCost(bestPair.optimalFrequency)){
                    bestPair = pair;
                }
            }

            pairs.add(bestPair);
            processors.remove(bestPair.processor);

            if(processors.size() == 0)
            {
                break;
            }

        }
        return new Solution(pairs);

    }
}
