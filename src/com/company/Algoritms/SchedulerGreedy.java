package com.company.Algoritms;

import com.company.*;
import com.company.Algoritms.Components.PairProcessorTask;
import com.company.Algoritms.Components.Solution;
import com.company.InputOutput.Reporter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SchedulerGreedy extends BaseScheduler {

    public SchedulerGreedy(ArrayList<SProcessor> processors, ArrayList<STask> tasks)
    {
        super(processors, tasks);
    }





    @Override
    protected String name() {
        return "SchedulerGreedy";
    }

    @Override
    protected Solution getBestSolution( ArrayList<SProcessor> processors, List<STask> independentArrivedTasks)
    {
        /**
         * We are sure that after sorting all the tasks will be sorted by their "deadlineCapCoefficient" value
         */
        Collections.sort(independentArrivedTasks, new Comparator<STask>() {
            @Override
            public int compare(STask o1, STask o2) {
                return Integer.compare(o1.deadlineCapCoefficient(), o2.deadlineCapCoefficient());
            }
        });



        List<Solution> solutions = new ArrayList<>();

        int procLength = processors.size();
        int tasksLength = independentArrivedTasks.size()<= procLength ? independentArrivedTasks.size() : procLength;
        int[] idArray;

        // array of processors ID
        idArray = new int[procLength];

        for(int i = 0; i < procLength; i++)
        {
            idArray[i] = processors.get(i).id;
        }

        // all the permuted possibilities
        List<List<Integer>> results = permute(idArray);

        for(int i = 0; i < results.size(); i+=1)
        {
            List<Integer> res2 = results.get(i);
            List<PairProcessorTask> pairs = new ArrayList<>(tasksLength);
            for(int j = 0; j < tasksLength; j++)
            {
                pairs.add(new PairProcessorTask(independentArrivedTasks.get(j), findProcessor(processors, res2.get(j))));
            }
            solutions.add(new Solution(pairs));
        }


        float min = Float.MAX_VALUE;
        float current;
        Solution best = null;

        for (Solution solution : solutions) {
            current = solution.getCost();
            if(current < min)
            {
                min = current;
                best = solution;
            }

        }


        return best;

    }
}
